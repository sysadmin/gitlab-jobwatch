import gitlab
import datetime
import dateutil
import dateutil.parser
import mysql.connector

# This isn't the cleanest way to do things, but we don't have another way of passing state around unfortunately
configuration = None
gitlabServer = None

# Updates the details we hold for the given Pipeline
def syncPipelineStatus( gitlabProjectId, gitlabPipelineId ):
    # First thing we need to grab is this pipeline from Gitlab
    # Start with the project itself...
    project = gitlabServer.projects.get( gitlabProjectId )
    # Then make our way across to the pipeline
    # We can lazy load the pipeline as we don't need any data except for the ID numbers to be present...
    pipeline = project.pipelines.get( gitlabPipelineId, lazy=True )

    # Now it is time to grab the data we actually care about: the jobs and the test reports
    jobs = pipeline.jobs.list()
    testReport = pipeline.test_report_summary.get()

    # Start by extracting the test results into a more useful form
    testsByJob = {}
    for testResult in testReport.test_suites:
        # Grab the job name this entry belongs to
        jobName = testResult['name']
        # Now bring the info over
        testsByJob[ jobName ] = testResult

    # Now we can start populating the database
    # First we need to connect...
    dbConnection = mysql.connector.connect(
        host=configuration['Database']['hostname'], 
        database=configuration['Database']['database'], 
        user=configuration['Database']['username'], 
        password=configuration['Database']['password']
    )
    dbCursor = dbConnection.cursor()
    
    # Next thing we need to do is make sure we have an entry for this project in the database
    # We normalise this to make some queries easier and scalable
    dbQuery  = """INSERT INTO projects (id, path) VALUES (%s, %s) ON DUPLICATE KEY UPDATE path=%s"""
    dbRecord = ( project.id, project.path_with_namespace, project.path_with_namespace )
    dbCursor.execute(dbQuery, dbRecord)
    
    # Now we start inserting details about jobs in...
    # Setup our query for this
    dbQuery = """
        REPLACE INTO jobs ( id, project_id, git_reference, job_name, commit_sha1, commit_author, created_at, finished_at, status, duration, tests_total, tests_passed, tests_failed, tests_other )
        VALUES ( %s, %s, %s, %s, %s, %s, %s,  %s, %s, %s, %s, %s, %s, %s )
    """
    
    # Start by going over the jobs
    for gitlabJob in jobs:
        # Turn our dates into something that MySQL understands
        # We have to do some special handling with finished_at as the job could still be in progress
        dbCreatedAt  = dateutil.parser.parse( gitlabJob.created_at )
        dbFinishedAt = datetime.datetime.utcfromtimestamp( 0 )

        if gitlabJob.finished_at is not None:
            dbFinishedAt = dateutil.parser.parse( gitlabJob.finished_at )
        
        # Retrieve the test counts if they exist
        # Start with some initial safe values...
        dbTestsTotal = 0
        dbTestsPass  = 0
        dbTestsFail  = 0
        dbTestsOther = 0
        
        # If it exists update the values
        if gitlabJob.name in testsByJob:
            dbTestsTotal = testsByJob[ gitlabJob.name ]['total_count']
            dbTestsPass  = testsByJob[ gitlabJob.name ]['success_count']
            dbTestsFail  = testsByJob[ gitlabJob.name ]['failed_count']
            dbTestsOther = testsByJob[ gitlabJob.name ]['skipped_count'] + testsByJob[ gitlabJob.name ]['error_count']
        
        # Assemble the updated record...
        dbRecord = (
            gitlabJob.id, # id
            project.id, # project_id
            gitlabJob.ref, # git_reference
            gitlabJob.name, # job_name
            gitlabJob.commit['id'], # commit_sha1
            gitlabJob.commit['author_name'], # commit_author
            dbCreatedAt.strftime('%Y-%m-%d %H:%M:%S'), # created_at
            dbFinishedAt.strftime('%Y-%m-%d %H:%M:%S'), # finished_at
            gitlabJob.status, # status
            gitlabJob.duration, # duration
            dbTestsTotal, # tests_total
            dbTestsPass, # tests_passed
            dbTestsFail, # tests_failed
            dbTestsOther # tests_other
        )
        
        # Now commit it to the database
        dbCursor.execute(dbQuery, dbRecord)
        
    # Finally commit everything we did
    dbConnection.commit()
    
    # All done!
    return True
