import json
import falcon

# Falcon Resource handler for Gitlab System Hook events
class ProjectHookResource(object):

    # Initial Setup
    def __init__(self, redisQueue, gitlabToken):
        # Store everything we need to operate
        self.redisQueue  = redisQueue
        self.gitlabToken = gitlabToken

    # Receive a submission from Gitlab
    def on_post(self, req, resp):
        # First check to make sure the submission comes from Gitlab
        if req.get_header('X-Gitlab-Token') != self.gitlabToken:
            raise falcon.HTTPForbidden('Your request is not valid.')

        # Now that we know we are dealing with a Gitlab System Hook submission, we can decode it
        # We expect to receive a JSON formatted payload
        try:
            body = req.stream.read(req.content_length or 0)
            print(body)
            projectEvent = json.loads(body.decode('utf-8'))
        except:
            raise falcon.HTTPBadRequest('Invalid Submission', 'Valid JSON documents are required.')

        # Is this a valid submission from Gitlab?
        if 'object_kind' not in projectEvent:
            raise falcon.HTTPBadRequest('Invalid Submission', 'Valid JSON documents are required.')

        # Is this a Pipeline event?
        if projectEvent['object_kind'] == 'pipeline':
            # Trigger the necessary sync
            self.redisQueue.enqueue('GitlabJobWatcher.WebEvents.syncPipelineStatus', projectEvent['project']['id'], projectEvent['object_attributes']['id'])

        # Otherwise maybe we have a Job event?
        if projectEvent['object_kind'] == 'build':
            # Perform the appropriate sync here too
            self.redisQueue.enqueue('GitlabJobWatcher.WebEvents.syncPipelineStatus', projectEvent['project_id'], projectEvent['pipeline_id'])

        # Prepare our response
        jsonResponse = {'message': 'OK'}

        # Give Gitlab the all clear that we've done what is needed
        resp.status = falcon.HTTP_200
        resp.body = json.dumps(jsonResponse)
