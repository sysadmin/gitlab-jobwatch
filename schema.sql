CREATE TABLE projects (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    path VARCHAR(100) NOT NULL
);

CREATE TABLE jobs (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    project_id INT UNSIGNED,
    git_reference VARCHAR(100) NOT NULL,
    job_name VARCHAR(100) NOT NULL,
    commit_sha1 VARCHAR(40) NOT NULL,
    commit_author VARCHAR(250) NOT NULL,
    created_at DATETIME NOT NULL,
    finished_at DATETIME NOT NULL,
    status VARCHAR(10) NOT NULL,
    duration INT UNSIGNED,
    tests_total INT UNSIGNED,   
    tests_passed INT UNSIGNED,
    tests_failed INT UNSIGNED, 
    tests_other INT UNSIGNED,
    INDEX ( project_id ),
    CONSTRAINT FOREIGN KEY ( project_id ) REFERENCES projects ( id )
);
