#!/usr/bin/env python
import os
import rq
import sys
import yaml
import redis
import gitlab
import argparse
import GitlabJobWatcher
import GitlabJobWatcher.WebEvents

# Gather the command line arguments we need
parser = argparse.ArgumentParser(description='Webservice worker to receive notifications from Gitlab and queue them for processing')
parser.add_argument('--config', help='Path to the configuration file to work with', required=True)
args = parser.parse_args()

# Make sure our configuration file exists
if not os.path.exists( args.config ):
    print("Unable to locate specified configuration file: %s".format(args.config))
    sys.exit(1)

# Read in our configuration
configFile = open( args.config, 'r' )
configuration = yaml.safe_load( configFile )

# Connect to the upstream Gitlab server we will be working with
# To do this, we need to get our credentials and hostname first
gitlabHost  = configuration['Gitlab']['instance']
gitlabToken = configuration['Gitlab']['token']
# Now we can actually connect
gitlabServer = gitlab.Gitlab( gitlabHost, private_token=gitlabToken )

# Ensure the configuration and gitlab server instances are available to workers
GitlabJobWatcher.WebEvents.gitlabServer  = gitlabServer
GitlabJobWatcher.WebEvents.configuration = configuration

# Connect to the Redis Server
redisSocket = configuration['Webservice']['redis-socket']
redisServer = redis.Redis( unix_socket_path=redisSocket )

# Determine the name of the queue we'll be processing...
queueName   = configuration['Webservice']['queue-name']

# Start processing our work...
worker = rq.Worker( queues=[queueName], connection=redisServer )
worker.work()
